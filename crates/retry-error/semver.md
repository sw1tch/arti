ADDED: fn fmt_error_with_sources
BREAKING: RetryError now requires its contained error types to impl std::error::Error
