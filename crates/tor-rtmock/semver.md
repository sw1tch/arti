ADDED: `MockExecutor`, `MockRuntime`
ADDED: `Default` impls for many types including several `Provider`s
BREAKING: `MockNet*` use an always-failing UDP stub, not unmocked system UDP
